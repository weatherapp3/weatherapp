import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-weather-component',
  templateUrl: './weather-component.component.html',
  styleUrls: ['./weather-component.component.css']
})
export class WeatherComponentComponent implements OnInit {

  weatherInfo: any;
  cityName = "";
  isShown = false;
  isError = false;

  constructor() { }

  ngOnInit(): void {
  }

  getPosition(): Promise<any>
  {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(resp => {
          resolve({lng: resp.coords.longitude, lat: resp.coords.latitude});
        },
        err => {
          reject(err);
        });
    });
  }

  setWeatherData(data: any) {
    this.weatherInfo = data;
    if(this.weatherInfo.message == "city not found") {
      this.isError = true;
      this.weatherInfo.errorMessage = "!! Please enter the correct city name !!";
    } else {
      this.isError = false;
      this.weatherInfo.temp_celcius = (this.weatherInfo.main.temp - 273.15).toFixed(0);
      this.weatherInfo.temp_min = (this.weatherInfo.main.temp_min - 273.15).toFixed(0);
      this.weatherInfo.temp_max = (this.weatherInfo.main.temp_max - 273.15).toFixed(0);
      this.weatherInfo.temp_feels_like = (this.weatherInfo.main.feels_like - 273.15).toFixed(0);
      this.weatherInfo.weatherDesc = this.weatherInfo.weather[0].description;
      this.cityName = this.weatherInfo.name;
    }
  }

  onCurrentLocation () {
    this.isShown = true;
    this.getPosition().then(pos=>
    {
      fetch('https://api.openweathermap.org/data/2.5/weather?lat=' + pos.lat + '&lon=' + pos.lng + '&appid=bb8bfe9f4b4b98efadca7f4a0e8a0c5b')
        .then(response => response.json())
        .then(data => {this.setWeatherData(data);})
    });
  }

  weatherOnSearch () {
    this.isShown = true;
    fetch('https://api.openweathermap.org/data/2.5/weather?q=' + this.cityName + '&appid=bb8bfe9f4b4b98efadca7f4a0e8a0c5b')
      .then(response => response.json())
      .then(data => {this.setWeatherData(data);});
  }

  clear() {
    this.isShown = false;
    this.cityName = "";
  }

}
